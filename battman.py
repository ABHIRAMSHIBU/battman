#!/usr/bin/env python3
#Copyleft 2019
#Author Abhiram Shibu
#Author Abhijith N Raj
import sys, termios, tty
import os
import time
import threading
from argparse import ArgumentParser
user=os.popen("whoami").read().strip()
runFlag=True
runThread=True
def EXIT(msg,errcode=0):
    global runThread
    print(msg)
    runThread=False
    exit(errcode)
    
def keyHandler():
    global runFlag
    while runThread:
        if keyboard.is_pressed('q'):  
            runFlag=False
            print("Bye..")
            exit() 
        else: 
            pass 
        time.sleep(0.01)
        
def main():
    # Will be intialised in the argument handling section
    infocsvwriter=None
    if(user == "root"):
        print("Success")
        try:
            import keyboard
        except:
            os.system("pip install keyboard")
        import keyboard
        keyHandler1=threading.Thread(target=keyHandler)
        parser=ArgumentParser()
        parser.add_argument('-n', help='Time in seconds before each refresh')
        parser.add_argument('-b','--battery', action='store_true', help='show battery status periodically')
        parser.add_argument('-c','--cpu',action='store_true', help='show cpu status periodically')
        parser.add_argument('-l','--log',action='store_true', help='log data to csv file')

        args = parser.parse_args()
        n=1
        if(args.__dict__["n"] ):
            if(float(args.__dict__["n"])==0):
                EXIT("Wait what the hell am I gonna do with n=0",errcode=-1)
            n=float(args.__dict__["n"])
        try:
            if(args.__dict__["log"]==True):
                # Logs will be saved in subdirectory called 'logs'
                log_dir = os.path.join(os.getcwd(),'logs')
                if not os.path.exists(log_dir):
                    os.makedirs(log_dir)
                # rotate the log
                for i in range(1,2):
                    if(os.path.exists(os.path.join(log_dir,"log"+str(i)+".csv"))):
                        os.rename(os.path.join(log_dir,"log"+str(i)+".csv"),os.path.join(log_dir,"log"+str(i+1)+".csv"))
                # special handling for log.csv to move to log1.csv
                if(os.path.exists(os.path.join(log_dir,"log.csv"))):
                    os.rename(os.path.join(log_dir,"log.csv"),os.path.join(log_dir,"log1.csv"))
                infocsvwriter = open(os.path.join(log_dir,"log.csv"),'w')
                infocsvwriter.write("Time,CPU,Battery\n")


            if(args.__dict__["battery"]==True):
                keyHandler1.start()
                os.chdir("/sys/class/power_supply/")
                directory=os.listdir()
                #Trying to check if there is atleast a battery
                if(len(directory)==0):
                        EXIT("You don't have a battery genius",errcode=-1)
                while(runFlag):
                    os.system("clear")
                    for i in directory:
                        dsgnPwr=None
                        currPwr=None
                        if("AC" not in i):
                            print("Battery, ",i)
                            if(os.path.exists(i+"/model_name")):
                                data=open(i+"/model_name").read()
                                print("Battery Model:",data.strip())
                            if(os.path.exists(i+"/capacity")):
                                data=open(i+"/capacity").read()
                                print("Battery Capacity:",data.strip(),"%")
                            if(os.path.exists(i+"/capacity_level")):
                                data=open(i+"/capacity_level").read()
                                print("Battery Level:",data.strip())
                            if(os.path.exists(i+"/energy_now")):
                                data=open(i+"/energy_now").read()
                                print("Power in Battery:",float(data.strip())/1000**2,"Wh")
                                if(infocsvwriter):
                                    # write the same into a csv file
                                    infocsvwriter.write("{},{}\n".format(str(time.time()).strip(), float(data.strip())/1000**2))
                                    infocsvwriter.flush()
                                    # TODO: Close when program ends
                            if(os.path.exists(i+"/energy_full")):
                                data=open(i+"/energy_full").read()
                                currPwr=float(data.strip())/1000**2
                                print("Power in Battery when full:",currPwr,"Wh")
                            if(os.path.exists(i+"/energy_full_design")):
                                data=open(i+"/energy_full_design").read()
                                dsgnPwr=float(data.strip())/1000**2
                                print("Power Design Battery:",dsgnPwr,"Wh")
                            if(os.path.exists(i+"/cycle_count")):
                                data=open(i+"/cycle_count").read()
                                print("Charge-Discharge Cycles:",data.strip())
                            if((currPwr is not None) and (dsgnPwr is not None)):
                                data=currPwr/dsgnPwr
                                data*=100
                                print('Battery Health:',data,"%")
                            if(os.path.exists(i+"/voltage_now")):
                                data=open(i+"/voltage_now").read()
                                print("Voltage :",float(data.strip())/1000**2,"V")
                            if(os.path.exists(i+"/status")):
                                data=open(i+"/status").read()
                                print("Battery Status :",data.strip())
                            print()
                    now = float(time.time())
                    while((float(time.time())-now)<n):
                        time.sleep(n/100)
                    os.system("clear")
            if(args.__dict__["cpu"]==True):
                keyHandler1.start()
                os.chdir("/sys/devices/system/cpu")
                directory=[]
                nproc=int(os.popen("nproc --all").read().strip())
                for i in range(nproc):
                    if(os.path.exists("cpu"+str(i))):
                        directory.append("cpu"+str(i))
                print(directory)
                while(runFlag):
                    os.system("clear")
                    for i in directory:
                        print("CPU thread/core:",i.strip("cpu"))
                        if(os.path.exists(i+"/cpufreq/scaling_cur_freq")):
                            data=open(i+"/cpufreq/scaling_cur_freq").read()
                            print("Current frequency:",float(data.strip())/1000**2,"GHz")
                        print()
                    now = float(time.time())
                    while((float(time.time())-now)<n):
                        time.sleep(n/100)
                EXIT("")
        except KeyboardInterrupt:
            EXIT("Bye..")
    else:
        print("Fail")
mainThread=threading.Thread(target=main)
mainThread.start()
